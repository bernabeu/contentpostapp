from WebApp import WebApp

class ContentPost(WebApp):
    def __init__(self, host, port):
        super().__init__(host, port)
        self.content_dict = {
            '/': '<html><body><h1>Main page</h1></body></html>',
            }

    def process(self, analyzed):
        resource = analyzed['recurso']
        method = analyzed['metodo']

        formulario = '<form method="post">' \
                     '<label>Introduce el recurso para guardar:' \
                     '<input name="name" autocomplete="name"/>' \
                     '</label>' \
                     '<button>Enviar</button>' \
                     '</form>'

        response = resource +" "+ formulario

        if method == 'GET':
            if resource in self.content_dict:
                http_code = "200 OK"
                html_content = "<html><body>La p&aacute;gina ya hab&iacute;a sido solicitada " \
                               "y guardada anteriormente"+response+ "<h1></h1></body></html>"
            else:
                http_code = "200 OK"
                html_content = "<html><body><h1>p&aacute;gina nueva:"+response+ "</h1></body></html>"
                self.content_dict[resource] = resource
            return http_code, html_content

        elif method == 'POST':
            http_code = "200 OK"
            self.content_dict[resource] = resource
            html_content = "<html><body><h1>p&aacute;gina:"+response+ "</h1></body></html>"

            return http_code, html_content


if __name__ == '__main__':
    web_app = ContentPost('', 1234)
    web_app.accept_clients()
